# DADA pipeline

To run this pipeline, download the necessary files for taxonomic assignment
(consult (https://benjjneb.github.io/dada2/tutorial.html)) and include their paths
in the R code.

Then, provide a list of files as the first argument and the amount of threads for the analyis
as the second argument.

Please note that this pipeline works with single-end data.
