library(tidyr)
library(dplyr)
library(forcats)
library(ggplot2)
library(RColorBrewer)
library(ggpubr)

source("../functions.R")

make_table = function(otus, assignment, r="Phylum"){
    all_names = c()
    for (n in 1:ncol(otus)){
        name = assignment[n,r]
        all_names = c(all_names, name)
    }
    all_names[is.na(all_names)] = "Unclassified"
    # for sample:
    all_samples = NULL
    for (n in 1:nrow(otus)){
        counts = otus[n,]; names(counts) = NULL
        result = data.frame(name = all_names,
                            counts = counts
                            ) %>%
                            group_by(name) %>%
                            summarize(counts = sum(counts)) %>%
                            tibble::add_column(sampleID = rownames(otus)[n])
        all_samples = rbind(all_samples, result)
    }
    return(as.data.frame(all_samples))
}

tax_from_species = function(species_table, desired_rank){
    merged = merge(species_table, taxlist, by.x="name", by.y="species")
    new_table = merged %>%
                    group_by(.dots=c(desired_rank,"sampleID")) %>%
                    summarize(relabundance=sum(relabundance),relabundance=sum(relabundance)) %>%
                    as.data.frame
    names(new_table) = c("name", "sampleID", "relabundance")
    colnames(new_table) = c("name", "sampleID", "relabundance")
    return(new_table)
}

otu = readRDS("../otu_table.rds")
tax_decipher = readRDS("../tax_decipher.rds")
family_16s = make_table(otu, tax_decipher, "Family")

kraken_fam = read_properly("kraken/family_good.txt")
mpa2_fam = read_properly("metaphlan2/metaphlan2_family.txt")
motu_species = read_properly("motu2/motu2.txt")
motu_fam = tax_from_species(motu_species, "family")
kaiju_fam = read_properly("kaiju/kaiju.family")

kraken_fam$software = "Kraken2"
mpa2_fam$software = "MetaPhlAn2"
motu_fam$software = "mOTUs2"
kaiju_fam$software = "Kaiju"

merged_fam = bind_rows(kraken_fam, mpa2_fam, kaiju_fam)
merged_fam$name = fct_recode(merged_fam$name, "Akkermansiaceae" = "Verrucomicrobiaceae")
family_16s$name = fct_expand(family_16s$name, "Clostridiaceae")
family_16s$name = fct_recode(family_16s$name, "Clostridiaceae" = "Clostridiaceae 1")

how_many = 15
top = (merged_fam %>% group_by(name) %>%
    summarize(total = sum(relabundance)) %>%
    arrange(desc(total)) %>%
    top_n(how_many))$name
top = rev(top)
color_palette = colorRampPalette(brewer.pal(8, "Set2"))(how_many)

merged_fam$name = as.character(merged_fam$name)
merged_fam$name[merged_fam$name %!in% top] = "Other"
#merged_fam$name[merged_fam$name == "Verrucomicrobiaceae"] = "Akkermansiaceae"
merged_fam$name = factor(merged_fam$name, levels=c("Other", as.vector(top)))

family_16s = family_16s[family_16s$name != "Unclassified",]
family_16s$name = as.character(family_16s$name)
#family_16s$name[family_16s$name == "Clostridiaceae 1"] = "Clostridiaceae"
family_16s$name[family_16s$name %!in% top] = "Other"
family_16s$name = factor(family_16s$name, levels=c("Other", as.vector(top)))

plot_shotgun = function(merged){
    ggplot(merged_fam, aes(x=software, y=counts, fill=name)) +
        geom_bar(stat="identity", position="fill") +
        scale_fill_manual(values = c(col_grey_na, color_palette)) +
        facet_wrap(~ sampleID) +
        theme(legend.position="right",
        axis.text.x=element_text(angle=60, hjust=1, vjust=1)) +
        labs(x=NULL, y=NULL, fill=NULL)
}

plot_16s = function(long){
    s = do.call(rbind,strsplit(long$sampleID, "_"))[,1:3]
    colnames(s) = c("sampleID", "regions", "source")
    long = cbind(long[,1:2], s)
    long$source = as.character(long$source)
    long$sr = paste(long$region, long$source, sep="_")
    long$sr = factor(long$sr, levels=
                     c("v2_stool", "v3_stool", "v4_stool", "v6v7_stool", "v7v8_stool",
                       "v2_tissue", "v3_tissue", "v4_tissue", "v6v7_tissue", "v7v8_tissue"))
    long$source[long$source == "tissue"] = "Tissue"
    long$source[long$source == "stool"] = "Stool"
    ggplot(long, aes(x=sr, y=counts, fill=name)) +
        geom_bar(stat="identity", position="fill") +
        facet_wrap(~ sampleID) +
        scale_fill_manual(values = c(col_grey_na, color_palette)) +
        theme(axis.text.x = element_text(angle = 60, hjust = 1, vjust = 1)) +
        labs(x=NULL, y=NULL, fill=NULL)
}

p1 = plot_shotgun(merged_fam)
p2 = plot_16s(family_16s)

pdf("descriptive.pdf", title="Descriptive at family level of microbiota composition", width=7, height=10, onefile=FALSE)
ggarrange(p1, p2, nrow=2, ncol = 1, common.legend = TRUE, legend = "right", labels=c("a", "b"))
dev.off()
