library(dplyr)
library(forcats)
library(reshape2)
library(CoDaSeq)
library(ggplot2)
library(ggrepel)
library(ggpubr)

source("../functions.R")

samples = c("AE1235","AE1236","AE1237","AE1238","AE1239","AE1240","AE1241","AE1242","AE1243")

otu = readRDS("../otu_table.rds")
tax_decipher = readRDS("../tax_decipher.rds")

decipher_family = make_table(otu, tax_decipher, "Family")
kraken = read_properly("kraken_family.txt")
kaiju = read_properly("kaiju_family.txt")
metaphlan = read_properly("metaphlan2_family.txt")

metaphlan$name = fct_recode(metaphlan$name, "Akkermansiaceae" = "Verrucomicrobiaceae")
decipher_family$name = fct_expand(decipher_family$name, "Clostridiaceae")
decipher_family$name = fct_recode(decipher_family$name, "Clostridiaceae" = "Clostridiaceae 1")
decipher_family =  filter(decipher_family, name != "Unclassified")

####################
## PCA OF SHOTGUN ##
####################

pca_1 = function(){
    d = bind_rows(long_to_wide(kraken, "counts"),
                  long_to_wide(kaiju, "counts"),
                  long_to_wide(metaphlan, "counts")
                  )
    d[is.na(d)] = 0


    m = data.frame(sampleID = rep(samples, 3),
                   classifier = c(rep("kraken", 9), rep("kaiju", 9), rep("metaphlan2", 9))
                   )

    rownames(d) = paste(m$sampleID, m$classifier, sep = "_")
    d.n0 = cmultRepl(d, label = 0, method = "CZM", output = "p-counts")
    d.subset = codaSeq.filter(d, min.reads = 500, min.prop = 0.005, samples.by.row = TRUE)
    d.n0 = cmultRepl(t(d.subset), label = 0, method = "CZM", output = "p-counts")
    d.n0.clr = codaSeq.clr(d.n0, samples.by.row = TRUE)

    pca = prcomp(d.n0.clr)
    p = pca_to_plot(pca, m)

    ggplot(p[[1]], aes(x = PC1, y = PC2, color = classifier, label=sampleID)) +
        geom_point() +
        geom_text_repel() +
        scale_color_discrete(name = "Classifier", labels = c("Kaiju", "Kraken 2", "MetaPhlAn2")) +
        labs_pca(p[[3]]) +
        theme_bw()
}

####################
## BIG PCA OF 16S ##
####################

pca_2 = function(){
    wide = long_to_wide(decipher_family)
    s = as.data.frame(do.call(rbind,strsplit(rownames(wide), "_"))[,1:3])
    names(s) = c("sampleID", "regions", "source")
    d = wide
    d.subset = codaSeq.filter(d, min.reads = 500, min.prop = 0.005, samples.by.row = TRUE)
    d.n0 = cmultRepl(t(d.subset), label = 0, method = "CZM", output = "p-counts")
    d.n0.clr = codaSeq.clr(d.n0, samples.by.row = TRUE)

    pca = prcomp(d.n0.clr)
    p = pca_to_plot(pca, s)

    ggplot(p[[1]], aes(x=PC1, y=PC2, color = regions, shape = source)) +
        geom_point() +
        scale_shape_discrete(name = "Source", labels = c("Stool", "Tissue")) +
        scale_color_discrete(name = "Region(s)", labels = c("V2", "V3", "V4", "V6-V7", "V7-V8")) +
        labs_pca(p[[3]]) +
        theme_bw()
}

#################################
## PCA OF TECHNICAL COMPARISON ##
#################################

pca_3 = function(){
    #metadata = data.frame(sampleID = rep(samples, 3),
                          #batch = c(rep("16S stool", 9), rep("16S tissue", 9), rep("shotgun stool", 9)))
    metadata = data.frame(sampleID = rep(samples, 2),
                          batch = c(rep("16S stool", 9), rep("shotgun stool", 9)))
    s = as.data.frame(do.call(rbind,strsplit(decipher_family$sampleID, "_"))[,1:3])
    names(s) = c("sampleID", "regions", "source")
    decipher_family$sampleID = NULL
    decipher_family = cbind(decipher_family, s)
    joined_16s = decipher_family %>%
        filter(regions %in% c("v4")) %>%
        group_by(.dots = c("name", "sampleID", "source")) %>%
        summarize(counts = sum(counts))
    stool_16s = filter(joined_16s, source == "stool"); stool_16s$source = NULL
    tissue_16s = filter(joined_16s, source == "tissue"); tissue_16s$source = NULL

    stool_16s_wide = long_to_wide(stool_16s)
    tissue_16s_wide = long_to_wide(tissue_16s)
    shotgun_wide = long_to_wide(kraken)

    shotgun_vs_16s = bind_rows(stool_16s_wide, shotgun_wide)
    shotgun_vs_16s[is.na(shotgun_vs_16s)] = 0
    rownames(shotgun_vs_16s) = c(paste(samples, "16S_stool", sep = "_"), paste(samples, "shotgun", sep = "_"))

    d.subset = codaSeq.filter(shotgun_vs_16s, min.reads = 500, min.prop = 0.005, samples.by.row = TRUE)
    d.n0 = cmultRepl(t(d.subset), label = 0, method = "CZM", output = "p-counts")
    d.n0.clr = codaSeq.clr(d.n0, samples.by.row = TRUE)

    pca = prcomp(d.n0.clr)
    p = pca_to_plot(pca, metadata)
    ggplot(p[[1]], aes(x = PC1, y = PC2, color = batch, label=sampleID)) +
        geom_point() +
        geom_text_repel() +
        scale_color_discrete(name = "Source", labels = c("16S stool", "Shotgun stool")) +
        labs_pca(p[[3]]) +
        theme_bw()
}

pdf(file = "dispersion.pdf", title = "PCAs of batch compositional effects", width = 10, height = 13)
ggarrange(
          pca_2(),
          ggarrange(pca_1(), pca_3(), nrow = 1, ncol = 2, labels = c("b", "c")),
          nrow = 2, ncol = 1, labels = "a"
          )
dev.off()
