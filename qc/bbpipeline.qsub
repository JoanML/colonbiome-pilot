#/bin/bash

#$ -cwd
#$ -S /bin/bash
#$ -pe smp 12
#$ -l mf=28G
#$ -N cleaning
#$ -t 1-9

module load apps/java-9.0.4
module load apps/bowtie2-2.3.4
module load apps/samtools-1.8
module load apps/bbmap-38.26
set -e

options=$1
filelist=$2

source $(echo $options)

if [ -z "$1" ]
  then
    echo "Please provide options as the first argument"
fi
if [ -z "$2" ]
  then
    echo "Please provide a filelist as the second argument"
fi

m1=$(sed "${SGE_TASK_ID}q;d" $filelist | cut -f1)
m2=$(sed "${SGE_TASK_ID}q;d" $filelist | cut -f2)
id=$(echo $m1 | awk -F "/" '{print $NF}' | cut -f1 -d "." | sed 's/R1//')

clumpify.sh \
    Xmx28g \
    in1=$m1 \
    in2=$m2 \
    out1=${temp}/${id}_deduped.m1.fq \
    out2=${temp}/${id}_deduped.m2.fq \
    dedupe \
    subs=1 \
    k=11 \
    passes=3 \
    &> ${temp}/${id}_deduplicationstats.txt

bbduk.sh \
    Xmx28g \
    in1=${temp}/${id}_deduped.m1.fq \
    in2=${temp}/${id}_deduped.m2.fq \
    out1=${out}/${id}_Q${quality}.m1.fq \
    out2=${out}/${id}_Q${quality}.m2.fq \
    ref=adapters,artifacts,phix \
    k=25 \
    mink=6 \
    hdist=1 \
    hdist2=0 \
    ktrim=r \
    qtrim=rl \
    trimq=$quality \
    minlength=75 \
    stats=${temp}/${id}_adapters.txt \
    dump=${temp}/${id}_kmers.txt \
    &> ${temp}/${id}_cleaning.txt
