## remove\_human.qsub

This script was used to remove human reads before submission to ENA.
Takes `options.sh` as the first argument and a list of files as second arguments.
In this list of files, mate1 and mate2 are on the same line, tab-separated

## bbpipeline.qsub

This script performs deduplication and quality trimming.
Input files work the same way as in `remove_human.qsub`.

## multiplesample.qsub

This script generates new FASTQ files of lower coverage from the original ones.
Takes a list of files as input, but this time like this (actual names don't matter,
just structure of the filelist):

    sample1_R1.fq
    sample1_R2.fq
    sample2_R1.fq
    sample2_R2.fq
