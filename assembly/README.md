# Metagenome assembly pipeline

Simply put, there are three steps on this pipeline: assembly (join reads into contigs),
binning (join contigs into putative genomes) and
quality control (check the quality of such bins)

## Assembly

We use the `run_metaspades.qsub` script, which takes as input two lists of files:
a list of mate1 and a list of mate2  (in the same order, obviously)

## Binning

We use the `run_metabat.qsub` script, which takes as input five lists (sorry!):

* A list of output folders
* mate1s (same as before)
* mate2s (same as before)
* A list of folders where the metagenomes are (from the previous step)
* A list of sample IDs

## Quality control

We use the `run_checkm.qsub` script, which takes as input three lists:

* A list of folders where the bins are (from the previous step)
* A list of output folders
* The extension of the files (I believe MetaBat uses `.fa` but I don't remember clearly)
