#!/share/apps/anaconda3/bin/python3

import math

sets = range(1,6)
samples = tuple(["AE" + str(n) for n in range(1235, 1244)])
sizes = ("50K", "100K", "500K", "1000K", "2500K", "5000K", "10000K", "15000K")
features = ("species", "genus", "phylum")


def filename(s, feature):
    f = "set" + str(s) + "/" + feature + ".txt"
    return f


def get_numbers(fh):
    numbers = {}
    next(fh)
    for line in fh:
        line = line.strip().split("\t")
        try:
            numbers[line[0]].append(int(line[3]))
        except KeyError:
            numbers[line[0]] = [int(line[3])]
    return numbers


def calc_shannon(list_of_numbers):
    shannon = 0
    total = math.fsum(list_of_numbers)
    for n in list_of_numbers:
        p = n/total
        s = p * math.log(p)
        shannon -= s
    return shannon


def compute_all_shannons(feature):
    all_shannons = []
    for s in sets:
        name = filename(s, feature)
        with open(name, "r") as fh:
            numbers = get_numbers(fh)
            for k, v in numbers.items():
                k = k.split("_")
                sample = k[0]
                size = k[-1]
                shannon = str(calc_shannon(v))
                out = "\t".join(["set" + str(s), sample, size, shannon])
                all_shannons.append(out)
    return all_shannons


for f in features:
    out = compute_all_shannons(f)
    with open(f + "_shannons.txt", "w") as fh:
        fh.write("set\tsampleID\tsize\tshannon\n")
        for o in out:
            fh.write(o + "\n")
