# Computing Shannon index in subsamples

This is the code we used to compute Shannon indices on the results of Kraken2 and HUMAnN2
for the subsampled FASTQs.

For simplicity, paths were hardcoded. The scripts expect inputs to be in set1, set2, etc.
folders. These input files are the output of the Kraken2 and HUMAnN2 pipelines in this
repository.
