#!/share/apps/anaconda3/bin/python3

import math

sets = range(1,6)
samples = tuple(["AE" + str(n) for n in range(1235, 1244)])
sizes = ("50K", "100K", "500K", "1000K", "2500K", "5000K", "10000K", "15000K")
features = ("genefamilies", "kegg", "pathabundance")

def filename(s, sample, size, feature):
    f = "set" + str(s) + "/" + sample + "_L008_nohuman_Q20_" + size +\
        ".concat_" + feature + ".tsv"
    return f


def get_numbers(fh):
    numbers = []
    next(fh)
    for line in fh:
        if "UNMAPPED" in line or \
                "UNINTEGRATED" in line or \
                "|" in line:
            continue
        numbers.append(float(line.strip().split("\t")[1]))
    return numbers


def calc_shannon(list_of_numbers):
    shannon = 0
    total = math.fsum(list_of_numbers)
    for n in list_of_numbers:
        p = n/total
        s = p * math.log(p)
        shannon -= s
    return shannon


def compute_all_shannons(feature):
    all_shannons = []
    for s in sets:
        for i in samples:
            for z in sizes:
                name = filename(s, i, z, feature)
                print("About to open", name)
                with open(name, "r") as fh:
                    shannon = calc_shannon(get_numbers(fh))
                    all_shannons.append((s, i, z, shannon))
    return all_shannons


for f in features:
    out = compute_all_shannons(f)
    with open(f + "_shannons.txt", "w") as fh:
        fh.write("set\tsampleID\tsize\tshannon\n")
        for o in out:
            fh.write("\t".join([str(n) for n in o]) + "\n")
