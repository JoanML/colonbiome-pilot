import os
import multiprocessing as mp


def divide_file(fullsize, nprocs):
    chunksize = int(fullsize/nprocs) + 1
    chunkstarts = range(0, fullsize, chunksize)
    return chunkstarts


class Seqs:
    def __init__(self, filename, nprocs):
        self.filename = filename
        self.fullsize = os.path.getsize(self.filename)
        self.nprocs = nprocs
        self.header = self.has_header()


    # header is boolean which indicates whether in the file containing
    # sequence IDs, those seqIDs start with @ or not
    def has_header(self):
        with open(self.filename) as fh:
            seqid = fh.readline()
            if seqid[0] == "@":
                return True
            else:
                return False


    # find the correct chunk start (avoid separating lines)
    def _find_start(self, fh, n):
        if n == 0:
            return 0
        fh.seek(n)
        fh.readline()
        return fh.tell()


    # do the correct file division using _find_start for each chunk
    def split_file(self):
        with open(self.filename, "r") as fh:
            self.starts = [self._find_start(fh,n) for n in divide_file(self.fullsize, self.nprocs)]
        self.ends = [n for n in self.starts][1:] + [self.fullsize]


# define task for the child processes
# input: chunk start byte, chunk end byte
# output: set of sequence ids
def extract_ids(filename, start, end, header):
    seqs = set()
    with open(filename) as fh:
        fh.seek(start)
        if header:
            while fh.tell() < end:
                seqs.add(fh.readline()[1:-1])  # remomve @ and \n
        else:
            while fh.tell() < end:
                seqs.add(fh.readline()[:-1])  # remove \n (header is False)
    return seqs


def get_ids(filename, threads):
    ids = Seqs(filename, threads)
    f = Seqs(filename, threads)
    f.split_file()
    args = [tuple([f.filename, f.starts[n], f.ends[n], f.header]) for n in range(threads)]
    with mp.Pool(threads) as p:
        result = p.starmap(extract_ids, args)
    seq_ids = set()
    seq_ids.update(*result)
    return seq_ids


# class sample: common stuff for both mated and unmated input
class Unmated:
    def __init__(self, m1, nprocs):
        self.m1 = m1
        self.m1_fullsize = os.path.getsize(self.m1)
        self.nprocs = nprocs


    # divide file in chunks by the amount of processors used
    # correct file division to not split different reads
    # we save the first seqid of each chunk to be able to keep pairing
    # in case there is a mate2
    def _find_start_m1(self, fh, n):
        if n == 0:
            first_id = fh.readline()
            return 0, first_id
        fh.seek(n)
        while True:
            line = fh.readline()
            if line == "+\n":
                break
        fh.readline()  # read quals and ignore
        n = fh.tell()
        first_id = fh.readline()  # read header and store
        return n, first_id


    # do the correct file division using _find_start_m1 for each chunk
    # first_seqids is ignored for unmated
    def split_mate1(self):
        starts = [None] * self.nprocs
        first_seqids= [None] * self.nprocs
        init_starts = divide_file(self.m1_fullsize, self.nprocs)
        with open(self.m1,"r") as fh1:
            for n in range(self.nprocs):
                result = self._find_start_m1(fh1, init_starts[n])
                starts[n] = result[0]
                first_seqids[n] = result[1]
        self.m1_starts = starts
        self.m1_ends = starts[1:] + [self.m1_fullsize]
        return first_seqids


class Mated(Unmated):
    def __init__(self, m1, m2, nprocs):
        super().__init__(m1, nprocs)
        self.m2 = m2
        self.m2_fullsize = os.path.getsize(m2)


    # each chunk needs to have the same reads in mate1 and mate 2
    # input: filehandle, initial byte start (gives an approximation of where we
    #        should look for the chunk start) and first sequence id of the chunk
    #        (which allows mate1 and mate2 to have the same reads)
    # output: corrected start position

    # we will look for the read in a range of +- 100000 bytes and keep expanding
    # the search using __str__.find()
    def _find_start_m2(self, fh, n, seqid):
        fh.seek(n)
        if n == 0:
            return n
        i = 100000
        while True:
            fh.seek(n-i)
            chunk = fh.read(i*2)
            pos = chunk.find(seqid)
            if pos >= 0:  # find returns 0 if it doesn't find, in which case no break
                break
            i += 100000
        return n -i + pos  # pos is relative to search space, not actual byte position


    # do the correct division using _find_start_m2
    def split_mate2(self, first_seqids):
        starts = [None] * self.nprocs
        ends = [None] * self.nprocs
        init_starts = divide_file(self.m2_fullsize, self.nprocs)
        with open(self.m2, "r") as fh2:
            for n in range(self.nprocs):
                starts[n] = self._find_start_m2(fh2,
                                                init_starts[n],
                                                first_seqids[n]
                                               )
        self.m2_starts = starts
        self.m2_ends = starts[1:] + [self.m2_fullsize]


# parallel task
# input: chunk start, chunk ends, ids
# output: fastq entries present in ids
def extract_unmated(filename, mate1_start, mate1_end, ids):
    m1_out = ""
    with open(filename, "r") as fh1:
        fh1.seek(mate1_start)
        while fh1.tell() < mate1_end:
            entry_m1 = [fh1.readline() for n in range(4)]
            id_good = entry_m1[0][1:-1]  # remove @ and \n
            if id_good in ids:
                m1_out += "".join(entry_m1)
    return m1_out


def extract_mated(mate1, mate2, mate1_start, mate1_end, mate2_start, mate2_end, ids):
    m1_out = ""
    m2_out = ""
    with open(mate1, "r") as fh1, open(mate2, "r") as fh2:
        fh1.seek(mate1_start)
        fh2.seek(mate2_start)
        while fh1.tell() < mate1_end and fh2.tell() < mate2_end:
            entry_m1 = [fh1.readline() for n in range(4)]
            entry_m2 = [fh2.readline() for n in range(4)]
            id_good = entry_m1[0][1:-1]  # remove @ and \n
            if id_good in ids:
                m1_out += "".join(entry_m1)
                m2_out += "".join(entry_m2)
    return m1_out, m2_out


def extract_seqids_unmated(filename, threads, seqids):
    s = Unmated(filename, threads)
    _ = s.split_mate1()
    args = [tuple([filename, s.m1_starts[n], s.m1_ends[n], seqids]) for n in range(threads)]
    with mp.Pool(threads) as p:
        result = p.starmap(extract_unmated, args)
    return "".join(result)


def extract_seqids_mated(mate1, mate2, threads, seqids):
    s = Mated(mate1, mate2, threads)
    first_seqids = s.split_mate1()
    s.split_mate2(first_seqids)
    args = [tuple([mate1, mate2, s.m1_starts[n], s.m1_ends[n], s.m2_starts[n], s.m2_ends[n], seqids])
            for n in range(threads)]
    with mp.Pool(threads) as p:
        result = p.starmap(extract_mated, args)
    m1_out = []
    m2_out = []
    for n in result:
        m1_out.append(n[0])
        m2_out.append(n[1])
    return "".join(m1_out), "".join(m2_out)
