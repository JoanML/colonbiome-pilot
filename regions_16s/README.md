# 16S IonTorrent analysis

This software will dig into IonTorrent sequencing reads and find out which variable region is
present in each read. It is divided in two blocks:

* find regions: Produce a BED-like file with region information for each read
* split regions: Using this information, generate new FASTQ files with only the regions you want

## Find regions

First, run `find_regions.qsub`. This script takes a list of filenames as input, and
will generate a BED-like `*.regions` file for each sample,
with information about the regions covered by your reads.
You may use it to obtain stats.

## Split regions

Once you have this `*.regions` file, you may run `split_regions.qsub`.
This script takes three inputs:

* List of filenames (same list as before)
* List of `*.regions` files (produced by the script before)
* List of regions you want. `find_regions.qsub`. For instance, for this analysis, we used:

    V2
    V3
    V4
    V6,V7
    V7,V8
