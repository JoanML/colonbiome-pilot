from collections import OrderedDict

comp = {"A":"T", "C":"G", "G":"C", "T":"A", "N":"N",
        "Y":"R", "R":"Y", "W":"W", "K":"M", "M":"K", "S":"S",
        "D":"H", "V":"B", "H":"D", "B":"V"}


def parse_regions(filename):
    fwd = OrderedDict()
    rev = OrderedDict()
    with open(filename, "r") as fh:
        n = 0
        for line in fh:
            n += 1
            if n % 2 == 1:
                line = line[1:-1].split("_")  # strip > and \n
                name = line[0]
                pos = tuple(line[1].split(":"))
            if n % 2 == 0:
                seq = line[:-1]
                rev_seq = seq[::-1]
                rev_comp = "".join([comp[n] for n in rev_seq])
                k1 = (name, (pos))
                k2 = (name + "_rev", (pos))
                fwd[k1] = seq
                rev[k2] = rev_comp
    return fwd, rev


def parse_variable_regions(filename):
    v_regions = {}
    with open(filename, "r") as fh:
        for line in fh:
            line = line.strip().split(",")
            v_regions[line[0]] = (int(line[1]), int(line[2]))
    return v_regions


translate = {"Y":("C","T"), "R":("A","G"), "W":("A","T"),  "K":("G","T"), "M":("C","A"),
             "S":("C","G"), "V":("A","C","G"), "H":("A","C","T"), "B":("C","G","T"),
             "D":("A","G","T"), "N":("A","C","G","T"), "A":"A", "C":"C", "G":"G", "T":"T"}
