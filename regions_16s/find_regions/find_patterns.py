#!/share/apps/anaconda3/bin/python3

import sys
import argparse
import logging
import multiprocessing as mp
from collections import OrderedDict

import pread
import configuration as cf

parser = argparse.ArgumentParser()
parser.add_argument("-i",
                    "--input",
                    dest = "input",
                    required = True,
                    help = "Unpaired FASTQ input"
                    )
parser.add_argument("-o",
                    "--output",
                    dest = "output",
                    required = True,
                    help = "Output file"
                    )
parser.add_argument("-t",
                    "--threads",
                    dest = "threads",
                    required = False,
                    type = int,
                    default = 1,
                    help = "Number of parallel tasks"
                    )
args = parser.parse_args()

# input: pattern and substr
# output: alignment score
def find_alignment_score(pattern, substr):
    score = 0
    assert len(pattern) == len(substr)
    for n in range(len(pattern)):
        if substr[n] in cf.translate[pattern[n]]:
            score += 1
    return score


# input: one pattern and full sequence
# output: tuple: 0 mismatches, tuple: start and end position OR None, None
def align_seqs_edge(pattern, seq):
    i = 0
    l = len(pattern)
    q = len(seq)
    while l - i > 9:
        left = find_alignment_score(pattern[i:l], seq[0:l-i])
        right = find_alignment_score(pattern[0:l-i], seq[q-l+i:q])
        i += 1
        if left >= l-i:
            return (0, (1, l-i))
        if right >= l-i:
            return (0, (q-l+i+1, q))
    return None, None


# input: one pattern and full sequence
# output: tuple: mismatches, tuple: start and end position
def align_seqs(pattern, seq):
    l = len(pattern)
    # scores = []
    maximum = 0
    for n in range(0, len(seq) - l , 1):
        score = find_alignment_score(pattern, seq[n:n+l])
        if score >= maximum:
            maximum = score
            pos = (n+1, n+l)  # n+1 transforms from 0-index to 1-index
    mismatches = l - maximum
    return (mismatches, pos)  # n of mismatches


# input: all patterns and full sequence
# output: interesting alignments
def search_alignment(patterns, seq, max_mismatches = 2):
    alignments = OrderedDict()
    edge_alignments = OrderedDict()
    for k, v in patterns.items():
        score, position = align_seqs(v, seq)
        score_edge, position_edge = align_seqs_edge(v, seq)
        if score <= max_mismatches:  # here is the threshold decision
            alignments[k] = position
        if score_edge is not None:
            edge_alignments[k] = position_edge
        all_alignments = alignments
        for k, v in edge_alignments.items():
            all_alignments[k] = v
    return all_alignments


# input: alignments
# output: E. coli positions in the read
def map_positions_fwd(alignments, read_length):
    keys = list(alignments.keys())
    region_start = int(keys[0][1][0])
    position = alignments[keys[0]][0]
    read_start = region_start - position + 1

    region_end = int(keys[-1][1][1])
    position = alignments[keys[-1]][1]
    read_end = region_end + read_length - position
    return read_start, read_end


def map_positions_rev(alignments, read_length):
    keys = list(alignments.keys())
    region_start = int(keys[0][1][0])
    position = alignments[keys[0]][1]
    read_start = region_start - read_length + position

    region_end = int(keys[-1][1][0])
    position = alignments[keys[-1]][0]
    read_end = region_end + position - 1
    return read_start, read_end


def which_regions(al_pos, v_regions):
    al = range(int(al_pos[0]), int(al_pos[1] +1))
    alignments = []
    for k, v in v_regions.items():
        if v[0] in al and v[1] in al:
            alignments.append(k)
        # reads that map partially to a region will be discarded
        if v[0] in al and v[1] not in al:
            return []
        if v[0] not in al and v[1] in al:
            return []
    return alignments


def parse_fastq(filename, start, end, v_regions, fwd, rev, max_len = 50):
    n, successes, too_short, no_alignments, fwd_and_rev, no_regions = 0, 0, 0, 0, 0, 0
    output = ""
    with open(filename, "r") as fh:
        fh.seek(start)
        while fh.tell() < end:
            n += 1
            header = fh.readline()
            seq = fh.readline()
            useless = fh.readline()
            quals = fh.readline()
            l = len(seq)

            if l <= max_len:
                too_short += 1
                continue

            fwd_alignments = search_alignment(fwd, seq[:-1], max_mismatches=2)
            rev_alignments = search_alignment(rev, seq[:-1], max_mismatches=2)

            if fwd_alignments and not rev_alignments:
                pos = map_positions_fwd(fwd_alignments, l)
                strand = "+"
            if rev_alignments and not fwd_alignments:
                pos = map_positions_rev(rev_alignments, l)
                strand = "-"

            if not fwd_alignments and not rev_alignments:
                no_alignments += 1
                continue
            if fwd_alignments and rev_alignments:
                fwd_and_rev += 1
                continue

            regions = which_regions(pos, v_regions)
            if regions == []:
                no_regions += 1
                continue

            successes += 1
            out = "\t".join([",".join(regions), str(pos[0]), str(pos[1]), header.strip(), "0", strand]) + "\n"
            output += out

    log = (n, successes, too_short, no_alignments, fwd_and_rev, no_regions)
    return output, log


def get_args(filename, starts, ends, v_regions, fwd, rev, nprocs, max_len = 50):
    task_args = []
    for n in range(nprocs):
        arg = (filename, starts[n], ends[n], v_regions, fwd, rev, max_len)
        task_args.append(arg)
    return task_args


def launch(function, arguments, nprocs):
    output = ""
    n, successes, too_short, no_alignments, fwd_and_rev, no_regions = 0, 0, 0, 0, 0, 0
    with mp.Pool(nprocs) as p:
        result = p.starmap(function, arguments)
    for r in result:
        output += r[0]
        n += r[1][0]
        successes += r[1][1]
        too_short += r[1][2]
        no_alignments += r[1][3]
        fwd_and_rev += r[1][4]
        no_regions += r[1][5]

    logging.info("Parsed " + str(n) + " sequences")
    logging.info("Classsified " + str(successes) + " sequences")
    logging.info("Discarded " + str(too_short) + " sequences for being too short")
    logging.info("Discarded " + str(no_alignments) + " sequences for having no alignment")
    logging.info("Discarded " + str(fwd_and_rev) + " sequences for having both forward and reverse alignments")
    logging.info("Discarded " + str(no_regions) + " sequences because read aligned properly but covered no regions entirely, or covered a region only partially.")
    return output

logging.basicConfig(level=logging.INFO, format='%(message)s')

module = sys.path[0]
forward, reverse = cf.parse_regions(module + "/conserved_regions.fasta")
v_regions = cf.parse_variable_regions(module + "/variable_regions.txt")

fastq = pread.Sample(args.input, args.threads)
fastq.split()

task_args = get_args(fastq.infile, fastq.starts, fastq.ends, v_regions,
                     forward, reverse, args.threads, max_len = 50)
output = launch(parse_fastq, task_args, args.threads)
with open(args.output, "w") as fh:
    fh.write(output)
