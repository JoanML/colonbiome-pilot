import os
import multiprocessing as mp


# class sample: common stuff for both mated and unmated input
class Sample:
    # def __init__(self, infile, outfile, function, arguments, nprocs):
    def __init__(self, infile, nprocs):
        self.infile = infile
        # self.out = outfile
        # self.function = function
        # self.arguments = arguments
        self.nprocs = nprocs


    # divide file in chunks by the amount of processors used
    def _divide_file(self):
        fullsize = os.path.getsize(self.infile)
        chunksize = int(fullsize/self.nprocs) + 1
        chunkstarts = range(0, fullsize, chunksize)
        return chunkstarts


    # correct file division to not split different reads
    def _find_start(self, fh, n):
        if n == 0:
            return n
        fh.seek(n)
        while True:
            line = fh.readline()
            if line == "+\n":
                break
        fh.readline()
        return fh.tell()


    # do the correct file division using _find_start1 for each chunk
    # first_seqids is ignored for unmated
    def split(self):
        chunkstarts = self._divide_file()
        starts = [None] * self.nprocs
        with open(self.infile,"r") as fh:
            for n in range(self.nprocs):
                starts[n] = self._find_start(fh, chunkstarts[n])
        ends = [n for n in starts][1:] + [os.path.getsize(self.infile)]
        self.starts = starts
        self.ends = ends
