# This file contains pipeline configuration.
# This configuration is not expected to change between runs
perl_module = "apps/perl-5.26.2"
kraken1_module = "apps/kraken-1.1"
kraken2_module = "apps/kraken2-2.0.8-beta"
bracken_module = "apps/bracken-2.2"
