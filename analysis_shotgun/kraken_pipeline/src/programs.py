import os
import subprocess
import re
import logging


# Load modules
def load_modules(perl_module, kraken_module, bracken_module):
    # adapted from 'man module'
    result = subprocess.run(['/usr/bin/modulecmd', 'python', 'purge'],
                            stdout=subprocess.PIPE, stderr=subprocess.PIPE
                            )
    exec(result.stdout)
    result = subprocess.run(['/usr/bin/modulecmd', 'python', 'load',
                             perl_module, kraken_module, bracken_module
                             ],
                            stdout=subprocess.PIPE, stderr=subprocess.PIPE
                            )
    exec(result.stdout)


# Create class sample
class Sample:
    def __init__(self, sampleID, mate1, mate2, database, threads, threshold,
                 read_length, kraken_args, bracken_args):
        self.taxonomies = ("P", "C", "O", "F", "G", "S")
        self.map_tax = {"P": "phylum", "C": "class", "O": "order",
                        "F": "family", "G": "genus", "S": "species"}
        self.sampleID = sampleID
        self.mate1 = mate1
        self.mate2 = mate2
        self.database = database
        self.threads = threads
        self.threshold = threshold
        self.read_length = read_length
        self.kraken_args = kraken_args
        self.bracken_args = bracken_args

# Run Kraken1 software, return the log
    def run_kraken1(self):
        command_kraken1 = ["kraken",
                           "--paired",
                           "--db", self.database,
                           "--threads", str(self.threads)] + \
                           self.kraken_args + \
                           [self.mate1, self.mate2]
        command_kraken1_filter = ["kraken-filter",
                                  "--db", self.database,
                                  "--threshold", str(self.threshold),
                                 ]
        command_kraken1_report = ["kraken-report",
                                  "--db", self.database,
                                  "--show-zeros",
                                  "temp/" + self.sampleID + ".kraken.out"
                                 ]
        logging.info("Running command: " + " ".join(command_kraken1))
        p1 = subprocess.run(command_kraken1, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        logging.info("Running command: " + " ".join(command_kraken1_filter))
        p2 = subprocess.run(command_kraken1_filter, input=p1.stdout, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        with open("temp/" + self.sampleID + ".kraken.out", "w") as fh:
            fh.write(p2.stdout.decode("utf-8"))
        logging.info("Running command: " + " ".join(command_kraken1_report))
        p3 = subprocess.run(command_kraken1_report, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        with open("temp/" + self.sampleID + ".kraken.report", "w") as fh:
            fh.write(p3.stdout.decode("utf-8"))
        log = p1.stderr.decode("utf-8")
        log = log.split("\r")[-1]
        return log

# Run Kraken2 software, return the log
    def run_kraken2(self):
        command_kraken2 = ["kraken2",
                           "--paired", self.mate1, self.mate2,
                           "--output", "temp/" + self.sampleID + ".kraken.out",
                           "--report", "temp/" + self.sampleID + ".kraken.report",
                           "--db", self.database,
                           "--threads", str(self.threads),
                           "--confidence", str(self.threshold),
                           "--report-zero-counts"
                          ] + \
                           self.kraken_args
        logging.info("Running command: " + " ".join(command_kraken2))
        result = subprocess.run(command_kraken2, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        log = result.stderr.decode("utf-8")
        log = log.split("\r")[-1]
        return log

# Run Bracken (no need to return anything)
    def run_bracken(self, output, taxonomy):
        command_bracken = ["bracken",
                           "-d", self.database,
                           "-i", "temp/" + self.sampleID + ".kraken.report",
                           "-o", "temp/" + output,
                           "-r", str(self.read_length),
                           "-t", "0",
                           "-l", taxonomy
                          ] + \
                           self.bracken_args
        logging.info("Running command: " + " ".join(command_bracken))
        subprocess.run(command_bracken, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

# Wrapper to make one Bracken run for each taxonomy (species, genus, etc.)
    def all_bracken_runs(self):
        for tax in self.taxonomies:
            tax_full = self.map_tax[tax]
            output_name = self.sampleID + "." + tax_full + ".txt"
            self.run_bracken(output_name, tax)


# Parse Kraken2 log file
def parse_log(sampleID, logfile):
    lines = logfile.split("\n")[-4:-1]
    pattern = re.compile("\d+")
    log_info = [sampleID,
                re.search(pattern, lines[0]).group(0),
                re.search(pattern, lines[1]).group(0),
                re.search(pattern, lines[2]).group(0)]
    return log_info
