# Kraken pipeline

To run this pipeline, call or submit `run_pipeline.sh`. Fill the arguments as follows:

* `-i`: Tab-separated list of inputs. The pipeline expects each sample in a different line,
with mate1 and mate2 separated by a tab character
* `-o`: Output folder
* `--db`: Database path
