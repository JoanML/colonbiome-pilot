#$ -S /bin/bash
#$ -N kraken-pipeline
#$ -cwd
#$ -pe smp 16
#$ -l mf=150G

kraken-pipeline \
    -i \
    -o \
    --threads 16 \
    --db \
    --threshold 0.1 \
    --read-length 150
