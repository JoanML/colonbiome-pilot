#!/bin/bash

taxonomy=$1

echo -e "sampleID\tname\tcounts\trelabundance" > metaphlan2_${taxonomy}.txt

if [[ $taxonomy == phylum ]]; then
    pattern="p__\w*\t\d.*"
fi
if [[ $taxonomy == class ]]; then
    pattern="c__\w*\t\d.*"
fi
if [[ $taxonomy == order ]]; then
    pattern="o__\w*\t\d.*"
fi
if [[ $taxonomy == family ]]; then
    pattern="f__\w*\t\d.*"
fi
if [[ $taxonomy == genus ]]; then
    pattern="g__\w*\t\d.*"
fi
if [[ $taxonomy == species ]]; then
    pattern="s__\w*\t\d.*"
fi
if [[ $taxonomy == strain ]]; then
    pattern="t__\w*\t\d.*"
fi

for file in *.metaphlan2; do
    grep -P -o "${pattern}" $file | while read line; do
        paste \
            <(echo ${file} | cut -f1 -d ".") \
            <(echo "${line}" | cut -f1 | sed 's/\w__//') \
            <(echo "${line}" | cut -f5) \
            <(echo "${line}" | cut -f2 | awk '{print $0/100}') \
            >> metaphlan2_${taxonomy}.txt
    done
done
