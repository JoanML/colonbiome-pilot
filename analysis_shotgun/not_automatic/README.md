# Non-automatic pipelines

To run Kaiju and MetaPhlAn2, submit the script in the output folder.
Provide a list of mate1s as first argument and a list of mate2s as second argument.
Make sure that those correspond to each other and are in order!
Modify the qsub parameter `-t` to control the amount of jobs (must match the amount of
samples)

Then, you may head to the output folder and run the parse scripts.
The `parse_metaphlan.sh` file takes an argument (species, genus, family...).
In our publication, it was analysed at family level.
