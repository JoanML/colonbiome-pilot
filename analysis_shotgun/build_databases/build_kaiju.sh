#/bin/bash

#$ -cwd
#$ -S /bin/bash
#$ -pe smp 12

module load apps/kaiju-1.6.3

db= # fill with database path

cd $db
makeDB.sh -p -t 12
