# Shotgun analysis

This is the code base I used for the analysis of clean shotgun sequences.
Kraken is run from an automatic Python pipeline, while Kaiju and MetaPhlAn2 are run normally.

Please note that databases must be built first. MetaPhlAn2 should be built on installation,
or necessary files can be downloaded later (there is no build process, just a download).
