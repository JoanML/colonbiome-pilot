# Functional analysis with HUMAnN2

These are the scripts used for functional analysis. Since we did this only for the subsampled data,
this is reflected in the directory layout (i.e. input and output folders contain subfolders named
set1, set2, etc.).

Three consecutive scripts are run:

* `concat_reads.qsub` conatenates mate1 and mate2 as suggested in the HUMAnN2 wiki
* `run_humann2_subsamples.qsub` runs the program
* `get_kegg.qsub` uses the map\_ko\_uniref.txt file to get KEGG ontologies from the UniRef90 results
