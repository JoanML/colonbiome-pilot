# Colonbiome pilot

Welcome! This repository contains the scripts for our Colonbiome pilot paper, now published in Scientific Data:

*Gut microbiome diversity detected by high-coverage 16S and shotgun sequencing of matched stool and colon biopsy samples*

https://www.nature.com/articles/s41597-020-0427-5

In this manuscript, we publish a data description of a pilot cohort of 9 human gut microbiome samples,
sequenced using shotgun and 16S technologies, from paired stool and tissue extracts.
Alongside this data, we publish the scripts we used for the analysis of this data.

We hope that this data and tools will be useful for researchers designing human gut microbiome experiments
or initiating in the bioinformatics analysis.

You will find READMEs in each folder explaining how to run these scripts.
Please, do not hesitate to contact me at jmas@idibell.cat should you have any questions or issues.
